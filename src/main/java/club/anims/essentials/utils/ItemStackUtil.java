package club.anims.essentials.utils;

import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

public class ItemStackUtil {
    public static ItemMeta addLore(ItemMeta meta, String... lore) {
        var loreList = meta.getLore();
        if(loreList == null)
            loreList = new ArrayList<>();
        loreList.addAll(Arrays.asList(lore));
        meta.setLore(loreList);
        return meta;
    }

    public static ItemMeta removeLore(ItemMeta meta, String... lore) {
        var loreList = meta.getLore();
        if(loreList == null)
            return meta;
        for(var line : lore)
            loreList.remove(line);
        meta.setLore(loreList);
        return meta;
    }

    public static boolean hasLore(ItemMeta meta, String... lore) {
        var loreList = meta.getLore();
        if(loreList == null)
            return false;
        for(var line : lore)
            if(!loreList.contains(line))
                return false;
        return true;
    }

    public static boolean hasLoreThatContains(ItemMeta meta, String... lore) {
        var loreList = meta.getLore();
        if(loreList == null)
            return false;
        for(var line : lore)
            if(loreList.stream().anyMatch(l -> l.contains(line)))
                return true;
        return false;
    }

    public static ItemMeta addLoreButSkipIfExists(ItemMeta meta, String... lore) {
        var loreList = meta.getLore();
        if(loreList == null)
            loreList = new ArrayList<>();
        for(var line : lore)
            if(!loreList.contains(line))
                loreList.add(line);
        meta.setLore(loreList);
        return meta;
    }
}
