package club.anims.essentials.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class NameCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        var server = sender.getServer();
        var player = server.getPlayer(sender.getName());

        if (player == null) {
            sender.sendMessage("You are not a player!");
            return true;
        }

        if(args.length == 0) {
            player.setDisplayName(player.getName());
            sender.sendMessage("Original name was restored!");
            return true;
        }

        var newName = ChatColor.translateAlternateColorCodes('&', String.join(" ", args));
        player.setDisplayName(newName);
        sender.sendMessage(String.format("Name changed to: %s", newName));

        return true;
    }
}