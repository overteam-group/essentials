package club.anims.essentials.commands;

import club.anims.essentials.utils.ItemStackUtil;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class MagicVeinCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        var server = sender.getServer();
        var player = server.getPlayer(sender.getName());

        if (player == null) {
            sender.sendMessage("You are not a player!");
            return true;
        }

        var item = player.getInventory().getItemInHand();

        if(item.getType().equals(Material.AIR)) {
            sender.sendMessage("You must be holding an item!");
            return true;
        }

        if(player.getLevel() < 10) {
            sender.sendMessage("You must be level 10 or higher to use this command!");
            return true;
        }

        player.setLevel(player.getLevel() - 10);

        var meta = item.getItemMeta();
        item.setItemMeta(ItemStackUtil.addLoreButSkipIfExists(meta, ChatColor.GOLD + "Magic Vein"));

        player.getInventory().setItemInHand(item);

        player.updateInventory();

        sender.sendMessage("Magic Vein applied!");
        return true;
    }
}
