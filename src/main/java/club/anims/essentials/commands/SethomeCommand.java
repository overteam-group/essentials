package club.anims.essentials.commands;

import club.anims.essentials.Essentials;
import club.anims.essentials.LocationData;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.HashMap;

public class SethomeCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        var server = sender.getServer();
        var player = server.getPlayer(sender.getName());

        if (player == null) {
            sender.sendMessage("You are not a player!");
            return true;
        }

        if(args.length == 0) {
            sender.sendMessage("You must specify a home name!");
            return true;
        }

        var homeName = args[0];
        var location = player.getLocation();

        var homes = Essentials.getInstance().getHomes();
        var playerHomes = homes.computeIfAbsent(player.getName(), k -> new HashMap<>());

        playerHomes.put(homeName, new LocationData(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch(), location.getWorld().getName()));

        sender.sendMessage(String.format("Home '%s' set!", homeName));

        Essentials.getInstance().dumpHomes();

        return true;
    }
}
