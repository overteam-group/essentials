package club.anims.essentials.commands;

import club.anims.essentials.Essentials;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BackCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        var server = sender.getServer();
        var player = server.getPlayer(sender.getName());

        if (player == null) {
            sender.sendMessage("You are not a player!");
            return true;
        }

        var location = Essentials.getInstance().getDeathLocations().computeIfAbsent(player.getName(), k -> null);

        if(location == null) {
            sender.sendMessage("You have no death location!");
            return true;
        }

        player.teleport(location.toLocation(player));
        sender.sendMessage("Teleported to your last death location!");

        return true;
    }
}
