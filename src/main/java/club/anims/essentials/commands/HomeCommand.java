package club.anims.essentials.commands;

import club.anims.essentials.Essentials;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.HashMap;

public class HomeCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        var server = sender.getServer();
        var player = server.getPlayer(sender.getName());

        if (player == null) {
            sender.sendMessage("You are not a player!");
            return true;
        }

        if(args.length == 0) {
            sender.sendMessage("You must specify a home name!");
            return true;
        }

        var homeName = args[0];
        var homes = Essentials.getInstance().getHomes();
        var playerHomes = homes.computeIfAbsent(player.getName(), k -> new HashMap<>());

        var homeLocation = playerHomes.get(homeName);

        if(homeLocation == null) {
            sender.sendMessage(String.format("Home '%s' does not exist!", homeName));
            return true;
        }

        player.teleport(homeLocation.toLocation(player));

        sender.sendMessage(String.format("Teleporting to home '%s'...", homeName));

        return true;
    }
}
