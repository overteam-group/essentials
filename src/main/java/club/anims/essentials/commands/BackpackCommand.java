package club.anims.essentials.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BackpackCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        var server = sender.getServer();
        var player = server.getPlayer(sender.getName());

        if(player == null) {
            sender.sendMessage("You are not a player!");
            return true;
        }

        player.openInventory(player.getEnderChest());

        return true;
    }
}
