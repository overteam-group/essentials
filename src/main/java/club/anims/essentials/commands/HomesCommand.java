package club.anims.essentials.commands;

import club.anims.essentials.Essentials;
import com.google.gson.Gson;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class HomesCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        var server = sender.getServer();
        var player = server.getPlayer(sender.getName());

        if (player == null) {
            sender.sendMessage("You are not a player!");
            return true;
        }

        sender.sendMessage("Your homes:");
        try{
            Essentials.getInstance().getHomes().get(player.getName()).forEach((k, v) -> {
                sender.sendMessage(String.format("%s %s: %s", k, new Gson().toJson(v), v.isOld() ? "[OLD]" : ""));
            });
        }catch (Exception e){
            sender.sendMessage("You have no homes!");
        }

        return true;
    }
}
