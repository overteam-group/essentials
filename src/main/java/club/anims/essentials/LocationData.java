package club.anims.essentials;

import lombok.*;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class LocationData {
    private double x, y, z;
    private float yaw, pitch;
    private String worldName = "";

    public Location toLocation(Player player) {
        World world;
        if(worldName.isBlank())
            world = player.getWorld();
        else
            world = player.getServer().getWorld(worldName);
        return new Location(world, x, y, z, yaw, pitch);
    }

    public boolean isOld() {
        return worldName.isBlank() || (yaw== 0.0f && pitch == 0.0f);
    }
}
