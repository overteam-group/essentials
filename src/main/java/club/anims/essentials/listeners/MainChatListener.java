package club.anims.essentials.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class MainChatListener implements Listener {
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onChat(AsyncPlayerChatEvent event){
        var player = event.getPlayer();
        var playerDisplayName = player.getDisplayName();

        var message = ChatColor.translateAlternateColorCodes('&', event.getMessage());
        event.setMessage(message);

        event.setFormat(ChatColor.translateAlternateColorCodes('&', String.format("%s&r: %s", playerDisplayName, message)));
    }
}