package club.anims.essentials.listeners;

import club.anims.essentials.Essentials;
import club.anims.essentials.LocationData;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class MainDeathListener implements Listener {
    @EventHandler(ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent event){
        var message = String.format("%s &rhas shuffled off this &6mortal coil!", event.getEntity().getDisplayName());
        message = ChatColor.translateAlternateColorCodes('&', message);

        event.setDeathMessage(message);

        event.getEntity().sendMessage("Type /back to return to your location of your last death.");

        Essentials.getInstance().getDeathLocations().put(
                event.getEntity().getName(),
                new LocationData(
                        event.getEntity().getLocation().getX(),
                        event.getEntity().getLocation().getY(),
                        event.getEntity().getLocation().getZ(),
                        event.getEntity().getLocation().getYaw(),
                        event.getEntity().getLocation().getPitch(),
                        event.getEntity().getLocation().getWorld().getName()
                )
        );
    }
}
