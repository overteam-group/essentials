package club.anims.essentials.listeners;

import club.anims.essentials.Essentials;
import club.anims.essentials.utils.ItemStackUtil;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class MainBlockDestroyingListener implements Listener {
    @EventHandler
    public void onBlockDestroy(BlockBreakEvent event) {
        var player = event.getPlayer();
        var hands = List.of(player.getInventory().getItemInMainHand(), player.getInventory().getItemInOffHand());

        var block = event.getBlock();
        //check if block is any kind of log and if player has a magic harvest item in hand
        if (!hands.stream().filter(ItemStack::hasItemMeta).anyMatch(item -> ItemStackUtil.hasLoreThatContains(item.getItemMeta(), "Magic Vein"))) return;

        var veinMaxAmount = 0;
        var blockTypeName = block.getType().name();
        if (blockTypeName.contains("LOG")){
            veinMaxAmount = 3*64;
        }else if(blockTypeName.contains("ORE")){
            veinMaxAmount = 2*64;
        }else{
            veinMaxAmount = (int) (64*(3.0f/8.0f));
        }

        var blocks = new ArrayList<Block>();
        blocks.add(block);

        try{
            for(var i=0; i<veinMaxAmount; i++){
                var currentBlock = blocks.get(i);
                //get blocks around block
                for(var x=-1; x<=1; x++){
                    for(var y=-1; y<=1; y++){
                        for(var z=-1; z<=1; z++){
                            var blockAround = currentBlock.getRelative(x,y,z);
                            if(blockAround.getType().name().equals(blockTypeName) && !blocks.contains(blockAround)){
                                blocks.add(blockAround);
                            }
                        }
                    }
                }
            }
        }catch (Exception ignored){}

        blocks.forEach(block1 -> {
            if(!block1.getType().equals(Material.AIR))
                block1.breakNaturally();
        });
    }
}
