package club.anims.essentials.listeners;

import club.anims.essentials.utils.ItemStackUtil;
import org.bukkit.CropState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Crops;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

public class MainBlockHarvestingListener implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void onBlockDestroy(BlockBreakEvent event) {
        AtomicBoolean hasRightItemInHand = new AtomicBoolean(false);
        var cropsList = new String[]{"POTATOES", "CARROTS", "BEETROOTS", "WHEAT"};

        var block = event.getBlock();
        var player = event.getPlayer();

        var hands = new ItemStack[]{player.getInventory().getItemInMainHand(), player.getInventory().getItemInOffHand()};

        Arrays.stream(hands).filter(ItemStack::hasItemMeta).filter(item -> ItemStackUtil.hasLoreThatContains(item.getItemMeta(), "Magic Harvest")).forEach(item -> {
            hasRightItemInHand.set(true);
        });

        if(!hasRightItemInHand.get()){
            return;
        }

        Arrays.stream(cropsList).toList().forEach(crop -> {
            if (block.getType().name().equals(crop)) {
                var blockState = block.getState();
                blockState.update(true);
                var crops = (Crops) blockState.getData();

                if (crops.getState() == CropState.RIPE) {
                    block.getDrops().forEach(item -> {
                        try{
                            player.getInventory().addItem(item);
                            player.updateInventory();
                        }catch (Exception e){
                            player.sendMessage("You cannot hold any more items in your inventory!");
                        }
                    });
                    crops.setState(CropState.SEEDED);
                    blockState.setData(crops);
                    blockState.update(true);
                }

                event.setCancelled(true);
            }
        });
    }
}