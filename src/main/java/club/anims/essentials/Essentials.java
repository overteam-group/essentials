package club.anims.essentials;

import club.anims.essentials.commands.*;
import club.anims.essentials.listeners.MainBlockDestroyingListener;
import club.anims.essentials.listeners.MainBlockHarvestingListener;
import club.anims.essentials.listeners.MainChatListener;
import club.anims.essentials.listeners.MainDeathListener;
import com.google.gson.Gson;
import lombok.Getter;
import org.apache.commons.io.FileUtils;
import org.bukkit.plugin.java.JavaPlugin;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

public final class Essentials extends JavaPlugin {
    @Getter
    private static Essentials instance;

    @Getter
    private final HashMap<String, HashMap<String, LocationData>> homes = new HashMap<>(); // playerName - [homeName - location]

    @Getter
    private final HashMap<String, LocationData> deathLocations = new HashMap<>(); // playerName - location

    @Override
    public void onEnable() {
        instance = this;
        loadHomes();
        getServer().getPluginManager().registerEvents(new MainChatListener(), this);
        getServer().getPluginManager().registerEvents(new MainDeathListener(), this);
        getServer().getPluginManager().registerEvents(new MainBlockHarvestingListener(), this);
        getServer().getPluginManager().registerEvents(new MainBlockDestroyingListener(), this);
        getCommand("sethome").setExecutor(new SethomeCommand());
        getCommand("delhome").setExecutor(new DelhomeCommand());
        getCommand("home").setExecutor(new HomeCommand());
        getCommand("homes").setExecutor(new HomesCommand());
        getCommand("name").setExecutor(new NameCommand());
        getCommand("bye").setExecutor(new ByeCommand());
        getCommand("back").setExecutor(new BackCommand());
        getCommand("backpack").setExecutor(new BackpackCommand());
        getCommand("magicharvest").setExecutor(new MagicHarvestCommand());
        getCommand("magicvein").setExecutor(new MagicVeinCommand());
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        dumpHomes();
    }

    public void dumpHomes() {
        try{
            var homesPath = Path.of("homes");
            if (!Files.exists(homesPath)) {
                Files.createDirectory(homesPath);
            }
            for (var playerName : homes.keySet()) {
                var playerHomes = homes.get(playerName);
                for (var homeName : playerHomes.keySet()) {
                    var location = playerHomes.get(homeName);
                    var file = Path.of("homes", playerName + "-" + homeName + ".json");
                    if(!Files.exists(file)) {
                        Files.createFile(file);
                    }
                    var json = new Gson().toJson(location);
                    FileUtils.writeStringToFile(file.toFile(), json, Charset.defaultCharset());
                    this.getLogger().info("Dumped " + playerName + "-" + homeName + " to " + file);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void loadHomes(){
        try{
            for (var file : Files.list(Path.of("homes")).toArray()) {
                var finalFile = (Path) file;
                var playerName = finalFile.getFileName().toString().split("-")[0];
                var homeName = finalFile.getFileName().toString().split("-")[1].split("\\.")[0];
                var json = FileUtils.readFileToString(finalFile.toFile(), Charset.defaultCharset());
                var location = new Gson().fromJson(json, LocationData.class);
                var playerHomes = homes.computeIfAbsent(playerName, k -> new HashMap<>());
                playerHomes.put(homeName, location);
                this.getLogger().info("Loaded " + playerName + "-" + homeName + " from " + finalFile);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
